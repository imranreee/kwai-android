package run_automation;

import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import utils.ReadExcel;
import utils.ReadNumber;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class KwaiAutomation {
    WebDriver driver;
    String excelPath = System.getProperty("user.dir") +"\\excel_file\\input.xlsx";
    String from;
    {
        try {
            from = ReadExcel.readData(excelPath, "1", 0, "1");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    String to;
    {
        try {
            to = ReadExcel.readData(excelPath, "1", 1, "1");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    int rowNo = 1;
    int counter = 0;
    AppiumDriverLocalService appiumService;
    String appiumServiceUrl;
    String appPackageFiles = "com.google.android.apps.nbu.files";
    String appActivityFiles = "com.google.android.apps.nbu.files.home.HomeActivity";

    @BeforeClass
    public void copyVdo(){
        File source = new File(from);
        File dest = new File(to);

        try {
            FileUtils.copyDirectory(source, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Video copied successfully!");
    }

    @BeforeTest
    public void connectEEmu() throws IOException, InterruptedException {
        //System.out.println("*******Starting the Appium server********");
        appiumService = AppiumDriverLocalService.buildDefaultService();
        appiumService.start();
        appiumServiceUrl = appiumService.getUrl().toString();
        //System.out.println("Appium Service Address: "+ appiumServiceUrl);
        appiumService.clearOutPutStreams();

        Thread.sleep(2000);

        Runtime.getRuntime().exec("adb connect localhost:21503");
    }

    @Test(invocationCount = 5) //How many account you want to use, put the count
    public void postVideo() throws Exception {
        int vdoAndTagRow = rowNo;

        DesiredCapabilities dc1 = new DesiredCapabilities();
        dc1.setCapability("platformName", "android");
        dc1.setCapability("deviceName", "OnePlusOne");
        dc1.setCapability("noReset", "false");
        dc1.setCapability("autoGrantPermissions", "true");
        dc1.setCapability("appPackage", "com.kwai.video");
        dc1.setCapability("appActivity", "com.yxcorp.gifshow.homepage.HomeActivity");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc1);

        By loginWithPhone = By.id("com.kwai.video:id/login_phone_text");
        By openCountryCode = By.id("com.kwai.video:id/tv_country_code");
        By searchCountryCode = By.id("com.kwai.video:id/editor");
        By searchResult = By.xpath("//android.widget.ListView/android.widget.LinearLayout/android.widget.LinearLayout");
        By phoneNumberField = By.id("com.kwai.video:id/edit_text");
        By nextBtn = By.xpath("//android.widget.Button[@text = 'Next']");
        By loginWithPassword = By.id("com.kwai.video:id/login_password");
        //By loginWithPassword = By.xpath("//android.widget.TextView[@text = 'Log in with Password']");
        By passwordField = By.id("com.kwai.video:id/edit_text");
        By postBtn = By.xpath("//android.view.ViewGroup[3]/android.widget.ImageView");
        By albumBtn = By.id("com.kwai.video:id/iv_album");
        By videoTab = By.xpath("//android.widget.TextView[@text = 'Video']");
        By selectVideo = By.xpath("//android.view.ViewGroup[1]/android.widget.ImageView[1]");
        By okBtn = By.id("com.kwai.video:id/right_btn");
        By nextBtn2 = By.id("com.kwai.video:id/next_step_button");
        By captionField = By.id("com.kwai.video:id/editor");
        By shareBtn = By.id("com.kwai.video:id/post");
        By shareVdo = By.id("com.kwai.video:id/share_btn_0");
        By downloadFolder = By.xpath("//android.widget.TextView[@text = 'Downloads']");
        By downloadTab = By.xpath("//android.widget.LinearLayout[@content-desc = 'Download']");
        By overflowMenu = By.id("com.google.android.apps.nbu.files:id/drop_down_arrow");
        By searchIcon = By.id("com.google.android.apps.nbu.files:id/search_menu_item");
        By searchField = By.id("com.google.android.apps.nbu.files:id/search_box");
        By searchResultFiles = By.id("com.google.android.apps.nbu.files:id/search_suggestion_view");
        By shareWith = By.xpath("//android.widget.TextView[@text = 'Share']");
        By kwaiPro = By.xpath("//android.widget.TextView[@text = 'KwaiPro']");
        By internalStorage = By.xpath("//android.widget.TextView[@text = 'Internal storage']");
        By download = By.xpath("//android.widget.TextView[@text = 'Download']");

        waitForVisibilityOf(loginWithPhone);
        driver.findElement(loginWithPhone).click();

        waitForClickAbilityOf(openCountryCode);
        driver.findElement(openCountryCode).click();

        waitForVisibilityOf(searchCountryCode);
        driver.findElement(searchCountryCode).sendKeys("+260");

        Thread.sleep(2000);
        waitForVisibilityOf(searchResult);
        driver.findElement(searchResult).click();

        waitForVisibilityOf(phoneNumberField);
        driver.findElement(phoneNumberField).clear();
        String phoneNumber = ReadExcel.readData(excelPath, "0", rowNo, "2");
        System.out.println("Account: "+phoneNumber);
        driver.findElement(phoneNumberField).sendKeys(phoneNumber);

        waitForVisibilityOf(nextBtn);
        driver.findElement(nextBtn).click();

        waitForVisibilityOf(loginWithPassword);
        driver.findElement(loginWithPassword).click();

        waitForVisibilityOf(passwordField);
        int password = ReadNumber.readData(excelPath, 0, rowNo, 3);
        driver.findElement(passwordField).sendKeys(String.valueOf(password));

        waitForVisibilityOf(nextBtn);
        driver.findElement(nextBtn).click();

        waitForVisibilityOf(postBtn);
        Thread.sleep(2000);

        for (int i = 0; i < 5; i++){
            try {
                Activity activity = new Activity(appPackageFiles, appActivityFiles);
                ((AndroidDriver) driver).startActivity(activity);

                /*if (counter < 1){
                    counter += 1;

                }*/

                waitForClickAbilityOf(internalStorage);
                driver.findElement(internalStorage).click();

                waitForClickAbilityOf(download);
                driver.findElement(download).click();
                Thread.sleep(3000);

                ((AndroidDriver) driver).pressKey(new KeyEvent().withKey(AndroidKey.BACK));
                Thread.sleep(1000);
                ((AndroidDriver) driver).pressKey(new KeyEvent().withKey(AndroidKey.BACK));

                waitForVisibilityOf(searchIcon);
                driver.findElement(searchIcon).click();

                waitForVisibilityOf(searchField);
                driver.findElement(searchField).click();
                String vdoName = ReadExcel.readData(excelPath, "0", vdoAndTagRow, "0");
                System.out.println("Video Name: "+vdoName);
                driver.findElement(searchField).sendKeys(vdoName);

                waitForClickAbilityOf(searchResultFiles);
                driver.findElement(searchResultFiles).click();

                waitForVisibilityOf(overflowMenu);
                driver.findElement(overflowMenu).click();

                waitForVisibilityOf(shareWith);
                driver.findElement(shareWith).click();

                waitForVisibilityOf(kwaiPro);
                driver.findElement(kwaiPro).click();

                Thread.sleep(5000);
                waitForClickAbilityOf(okBtn);
                driver.findElement(okBtn).click();

                Thread.sleep(5000);
                waitForClickAbilityOf(nextBtn2);
                driver.findElement(nextBtn2).click();

                waitForVisibilityOf(captionField);
                String tags = ReadExcel.readData(excelPath, "0", vdoAndTagRow, "1");
                System.out.println("Hash tags: "+tags);
                driver.findElement(captionField).sendKeys(tags);

                waitForVisibilityOf(shareBtn);
                driver.findElement(shareBtn).click();

                Thread.sleep(20000);
                waitForVisibilityLong(shareVdo);
                System.out.println((i+1)+" no video posted successfully");
            } catch (Exception e) {
                e.printStackTrace();
            }
            vdoAndTagRow += 1;
        }
        rowNo += 5;
        driver.quit();
    }

    @AfterTest
    public void endAutomation() throws IOException {
        //System.out.println("Stop Appium service");
        appiumService.stop();
        FileUtils.cleanDirectory(new File(to));
        System.out.println("Deleted used video from emulator");
    }

    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 50);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected void waitForVisibilityLong(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 300);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected void waitForClickAbilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

}
