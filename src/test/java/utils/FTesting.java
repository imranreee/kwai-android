package utils;

import java.io.IOException;
import java.util.Arrays;

public class FTesting {
    public static void main(String args[]){
        String excelPath = System.getProperty("user.dir") +"\\ExcelFile\\input.xlsx";
        String from;

        {
            try {
                from = ReadExcel.readData(excelPath, "1", 0, "1");
                System.out.println(from);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String to;
        {
            try {
                to = ReadExcel.readData(excelPath, "1", 1, "1");
                System.out.println(to);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
