package utils;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ReadNumber {
    public static int readData(String excelPath, int sheetNum, int rowNum, int columnNum) throws IOException {
        XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(new File(excelPath)));
        int data = (int) wb.getSheetAt(sheetNum).getRow(rowNum).getCell(columnNum).getNumericCellValue();
        return data;
    }
}
